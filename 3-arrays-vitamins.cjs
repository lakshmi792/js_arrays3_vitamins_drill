const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];




// 1. Get all items that are available 
let allItems = items.filter((values) => {
    if (values.available) {
        return values;
    }
});
console.log(allItems);

// 2. Get all items containing only Vitamin C.

let itemsVitaminC = items.filter((vitamin) => {
    return vitamin.contains === "Vitamin C";
});
console.log(itemsVitaminC);

// 3. Get all items containing Vitamin A.
let itemsVitaminA = items.filter((elements) => {
    return elements.contains.includes("Vitamin A");
});
console.log(itemsVitaminA);

// 4. Group items based on the Vitamins that they contain in the following format:
//         {
//             "Vitamin C": ["Orange", "Mango"],
//             "Vitamin K": ["Mango"],
//         }

//         and so on for all items and all Vitamins.

let groupVitamins = items.reduce((accumulater, currentValue) => {
    const itemContains = currentValue.contains.split(',').map((element) => {

        if (accumulater.hasOwnProperty(element.trim())) {
            accumulater[element.trim()].push(currentValue.name)
        }
        else {
            accumulater[element.trim()] = [currentValue.name]
        }
    })
    return accumulater;
}, {})

console.log(groupVitamins);

// 5. Sort items based on number of Vitamins they contain.

let sortItems = items.sort((first, second) => {

    const firstValue = first.contains.split(',').length;
    const secondValue = second.contains.split(',').length;

    if (firstValue < secondValue) {
        return -1;
    }
    if (firstValue > secondValue) {
        return 1;
    }
    else {
        return 0;
    }
})

console.log(sortItems);




